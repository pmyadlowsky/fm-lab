#include <lua.h>

#include "clock.h"
#include "jack.h"

frame_t frame_clock = 0;

static int frame_clock_lua(lua_State *state) {
	lua_pushinteger(state, frame_clock);
	return 1;
	}

static int frame_usec_lua(lua_State *state) {
	lua_pushinteger(state,
		(usec_t)(frame_clock / (double)sampling_rate * 1000000.0));
	return 1;
	}

void clock_init(lua_State *lstate) {
	lua_pushcfunction(lstate, frame_clock_lua);
	lua_setglobal(lstate, "frame_clock");
	lua_pushcfunction(lstate, frame_usec_lua);
	lua_setglobal(lstate, "frame_usec");
	frame_clock = 0;
	return;
	}
