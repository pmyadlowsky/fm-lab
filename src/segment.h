#ifndef _SEGMENT_H
#define _SEGMENT_H

#include "vertex.h"

#define SEGMENT_LINEAR 1
#define SEGMENT_EXPONENTIAL 2
#define SEGMENT_SIN2 3

typedef struct segment {
	VERTEX *start;
	VERTEX *finish;
	int form;
	int first; // first segment in shape
	double width;
	double height;
	int (*interpolator)(struct segment *, double, double*);
	struct segment *link;
	struct segment *seq;
	} SEGMENT;

SEGMENT *get_segment(VERTEX *, VERTEX *, int, int);
void release_segment(SEGMENT *);
int interpolate(SEGMENT *, double, double *);
#endif
