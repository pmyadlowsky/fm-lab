#ifndef _VERTEX_H
#define _VERTEX_H

typedef struct vertex {
	double x;
	double y;
	struct vertex *link;
	} VERTEX;

VERTEX *get_vertex(double, double);
void release_vertex(VERTEX *);

#endif
