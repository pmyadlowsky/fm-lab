#ifndef _WAVE_H
#define _WAVE_H

#include <lua.h>
#include "shape.h"
#include "clock.h"

#define WAVE_NEW 0
#define WAVE_ACTIVE 1
#define WAVE_PENDING 2
#define WAVE_RELEASED 3

typedef struct wave {
	struct wave *modulators;
	SHAPE *carrier;
	SHAPE *amplitudes;
	frame_t clock;
	frame_t lead_in;
	frame_t start;
	lua_State *lstate;
	int mute;
	int action_pending;
	struct wave *state_prev;
	struct wave *state_next;
	int status;
	} WAVE;

WAVE *wave_new(SHAPE *, SHAPE *);
void wave_add_modulator(WAVE *, WAVE *);
void wave_add_channel(WAVE *, SHAPE *);
void mix_waves(unsigned int, double *);
void activate_window(frame_t, frame_t);
void wave_submit(WAVE *, usec_t);
void wave_init(lua_State *);
#endif
