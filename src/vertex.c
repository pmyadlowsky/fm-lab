#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include "vertex.h"

static VERTEX *pool = NULL;
static pthread_mutex_t pool_mutex;
static int need_init = 1;

static void inline vertex_init() {
	need_init = 0;
	pthread_mutex_init(&pool_mutex, NULL);
	return;
	}

VERTEX *get_vertex(double x, double y) {
	VERTEX *v;
	if (need_init) vertex_init();
	pthread_mutex_lock(&pool_mutex);
	if (pool == NULL) {
		v = (VERTEX *)malloc(sizeof(VERTEX));
		}
	else {
		v = pool;
		pool = v->link;
		}
	pthread_mutex_unlock(&pool_mutex);
	v->link = NULL;
	v->x = x;
	v->y = y;
	return v;
	}

void release_vertex(VERTEX *v) {
	pthread_mutex_lock(&pool_mutex);
	v->link = pool;
	pool = v;
	pthread_mutex_unlock(&pool_mutex);
	return;
	}

