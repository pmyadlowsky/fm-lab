#ifndef _CLOCK_H
#define _CLOCK_H

#include <lua.h>

typedef unsigned long long int frame_t;
typedef unsigned long long int usec_t;

extern frame_t frame_clock;

void clock_init(lua_State *);
#endif
