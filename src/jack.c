#include <stdio.h>
#include <stdlib.h>
#include <jack/jack.h>

#include "jack.h"
#include "wave.h"
#include "shape.h"
#include "clock.h"

static jack_port_t **output_ports;
static jack_client_t *client = NULL;
static int channels;
static double mixbuf[16];
static const char *client_name = "fmlab";
static SHAPE *volume = NULL;
static SHAPE *volume_swap = NULL;
static frame_t volume_clock;
unsigned int sampling_rate = 48000;

static double noise() {
	return (rand() / (double)RAND_MAX - 0.5) * 2.0;
	}

static int process(jack_nframes_t nframes, void *art) {
	jack_default_audio_sample_t *out[16];
	jack_nframes_t f;
	double vol;
	int i;
	for (i = 0; i < channels; i++) {
		out[i] = jack_port_get_buffer(output_ports[i], nframes);
		}
	activate_window(frame_clock, frame_clock + nframes);
	for (f = 0; f < nframes; f++) {
		frame_clock += 1;
		vol = shape_eval(volume, volume_clock / sampling_rate);
		volume_clock += 1;
		mix_waves(channels, mixbuf);
		for (i = 0; i < channels; i++) {
			out[i][f] = mixbuf[i] * vol; // + noise() * 0.02;
			}
		if (volume_swap != NULL) {
			shape_release(volume);
			volume = volume_swap;
			volume_clock = 0;
			volume_swap = NULL;
printf("NEW VOLUME\n");
show_shape(volume);
			}
		}
	return 0;
	}

static void hookup() {
	if (client == NULL) return;
	int i;
	int err;
	char source[256];
	char dest[256];
	for (i = 0; i < channels; i++) {
		sprintf(source, "%s:out%02d", client_name, i + 1);
		sprintf(dest, "system:playback_%d", i + 1);
		err = jack_connect(client, source, dest);
		if (err != 0) break;
		printf("hookup %s -> %s\n", source, dest);
		}
	return;
	}

static int hookup_lua(lua_State *state) {
	hookup();
	return 0;
	}

void jack_set_volume(SHAPE *shape) {
	volume_swap = shape;
	return;
	}

static int set_volume_lua(lua_State *state) {
	if (lua_isnumber(state, 1))
		jack_set_volume(shape_static((double)lua_tonumber(state, 1)));
	else jack_set_volume((SHAPE *)lua_touserdata(state, 1));
	lua_pop(state, 1);
	return 0;
	}

static int sampling_rate_lua(lua_State *state) {
	lua_pushinteger(state, sampling_rate);
	return 1;
	}

static void lua_links(lua_State *lstate) {
	lua_pushcfunction(lstate, hookup_lua);
	lua_setglobal(lstate, "sys_out");
	lua_pushcfunction(lstate, set_volume_lua);
	lua_setglobal(lstate, "master_volume");
	lua_pushcfunction(lstate, sampling_rate_lua);
	lua_setglobal(lstate, "sampling_rate");
	}

static int sampling_rate_change(jack_nframes_t new_rate, void *arg) {
	sampling_rate = new_rate;
	return 0;
	}

int jack_init(int _channels, lua_State *lstate) {
	jack_options_t options = JackNullOption;
	jack_status_t status;
	channels = _channels;
	client = jack_client_open(client_name, options, &status, NULL);
	if (client == NULL) {
		printf("jack open failed: 0x%04x\n", status);
		if (status & JackServerFailed)
			printf("unable to connect to JACK server\n");
		return 0;
		}
	if (status & JackServerStarted)
		printf("JACK server started\n");
	jack_set_process_callback(client, process, 0);
	jack_set_sample_rate_callback(client, sampling_rate_change, NULL);
	sampling_rate = jack_get_sample_rate(client);
	output_ports = (jack_port_t **)malloc(channels * sizeof(jack_port_t *));
	int i;
	char pname[1024];
	for (i = 0; i < channels; i++) {
		sprintf(pname, "out%02d", i + 1);
		output_ports[i] = jack_port_register(client, pname,
				JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
		}
	lua_links(lstate);
	volume = shape_static(1.0);
	volume_clock = 0;
	return 1;
	}

void jack_run(void) {
	jack_activate(client);
	printf("JACK client ready with %d channels\n", channels);
	return;
	}

void jack_shutdown(void) {
	if (client != NULL) jack_client_close(client);
	client = NULL;
	return;
	}
