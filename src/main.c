#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <time.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>

#include "shape.h"
#include "segment.h"
#include "wave.h"
#include "clock.h"
#include "jack.h"
#include "osc.h"

#define DEFAULT_SAMPLING_RATE 48000
#define DEFAULT_CHANNELS 2
#define DEFAULT_OSC_PORT 7770

static lua_State *lstate = NULL;

static void bail(int sig) {
	if (sig != 0) printf("caught signal %s\n", strsignal(sig));
	if (lstate != NULL) lua_close(lstate);
	lstate = NULL;
	osc_shutdown();
	jack_shutdown();
	exit(1);
	}

WAVE *make_wave() {
	SHAPE *amp = shape_new(0.0, 0.0001);
	shape_add_segment(amp, 1.0, 1.0, SEGMENT_EXPONENTIAL);
	shape_add_segment(amp, 2.0, 0.00001, SEGMENT_EXPONENTIAL);
	WAVE *wave = wave_new(shape_static(100), amp);
	amp = shape_new(0.0, 0.0001);
	shape_add_segment(amp, 1.99, 1.0, SEGMENT_EXPONENTIAL);
	shape_add_segment(amp, 2.0, 0.00001, SEGMENT_EXPONENTIAL);
	wave_add_channel(wave, amp);
	int modc = rand() % 1000 + 100;
	SHAPE *beta = shape_new(0.0, 0.0001);
	shape_add_segment(beta, 1.0, 5.0, SEGMENT_SIN2);
	shape_add_segment(beta, 2.0, 0.0001, SEGMENT_SIN2);
	WAVE *mod = wave_new(shape_static(modc), beta);
	wave_add_modulator(wave, mod);
	mod = wave_new(shape_static(90), shape_static(0.5));
	wave_add_modulator(wave, mod);
	return wave;
	}

int main(int argc, char **argv) {
	char opt;
	fd_set rfds;
	int channels = DEFAULT_CHANNELS;
	char *script = NULL;
	int osc_port = DEFAULT_OSC_PORT;
	while ((opt = getopt(argc, argv, "c:s:O:")) != -1) {
		switch(opt) {
			case 'c':
				channels = atoi(optarg);
				break;
			case 's':
				script = strdup(optarg);
				break;
			case 'O':
				osc_port = atoi(optarg);
				break;
			}
		}
	signal(SIGINT, bail);
	signal(SIGTERM, bail);
	srand(time(NULL));
	lstate = luaL_newstate();
	luaL_openlibs(lstate);
	lua_pushinteger(lstate, channels);
	lua_setglobal(lstate, "channels");
	clock_init(lstate);
	shape_init(lstate);
	wave_init(lstate);
	osc_init(lstate, osc_port);
	int osc_fd = osc_get_fd();
	jack_init(channels, lstate);
	jack_run();
	char line[1024];
	if (script != NULL) {
		printf("load script %s\n", script);
		luaL_loadfile(lstate, script);
		free(script);
		lua_pcall(lstate, 0, 0, 0);
		}
	while (1) {
		FD_ZERO(&rfds);
		FD_SET(fileno(stdin), &rfds);
		FD_SET(osc_fd, &rfds);
		int ret = select(osc_fd + 1, &rfds, NULL, NULL, NULL);
		if (ret < 0) {
			printf("select error\n");
			continue;
			}
		else if (ret == 0) {
			printf("timeout\n");
			continue;
			}
		if (FD_ISSET(fileno(stdin), &rfds)) {
			printf("lua> ");
			fflush(stdout);
			if (fgets(line, sizeof(line), stdin) == NULL) break;
			if (luaL_loadbuffer(lstate, line, strlen(line), "line")) {
				printf("LUA PARSE: %s\n", lua_tostring(lstate, -1));
				lua_pop(lstate, 1);
				}
			else if (lua_pcall(lstate, 0, 1, 0)) {
				printf("LUA EXEC: %s\n", lua_tostring(lstate, -1));
				lua_pop(lstate, 1);
				}
			else lua_pop(lstate, lua_gettop(lstate));
			}
		if (FD_ISSET(osc_fd, &rfds)) {
			osc_process();
			}
		}
	bail(0);
	return 0;
	}
