#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <pthread.h>
#include <lua.h>
#include <lauxlib.h>
#include "wave.h"
#include "clock.h"
#include "jack.h"

#define INSERT_BEFORE 0
#define INSERT_AFTER 1

static WAVE *active_waves = NULL;
static WAVE *pending_waves = NULL;
static WAVE *pool = NULL;
static pthread_mutex_t pool_mutex;
static pthread_mutex_t active_mutex;
static pthread_mutex_t pending_mutex;
static const char *action_ref_table = "action_ref";

static WAVE *insert_wave(WAVE *w, WAVE *neighbor, int mode, WAVE **list) {
	if (*list == NULL) {
		w->state_prev = NULL;
		w->state_next = NULL;
		*list = w;
		}
	else if (mode == INSERT_BEFORE) {
		w->state_next = neighbor;
		if (neighbor->state_prev != NULL) {
			w->state_prev = neighbor->state_prev;
			neighbor->state_prev->state_next = w;
			}
		else {
			w->state_prev = NULL;
			*list = w;
			}
		neighbor->state_prev = w;
		}
	else { // insert after
		if (neighbor->state_next != NULL) {
			insert_wave(w, neighbor->state_next, INSERT_BEFORE, list);
			}
		else {
			neighbor->state_next = w;
			w->state_prev = neighbor;
			w->state_next = NULL;
			}
		}
	return w;
	}

static WAVE *remove_wave(WAVE *w, WAVE **list) {
	if (w->state_prev == NULL) {
		// top of list
		*list = w->state_next;
		if (w->state_next != NULL)
			w->state_next->state_prev = NULL;
		}
	else if (w->state_next == NULL) {
		// bottom of list
		w->state_prev->state_next = NULL;
		}
	else {
		// middle of list
		w->state_prev->state_next = w->state_next;
		w->state_next->state_prev = w->state_prev;
		}
	w->state_next = NULL;
	w->state_prev = NULL;
	return w;
	}

void wave_add_modulator(WAVE *w, WAVE *m) {
	m->state_next = w->modulators;
	w->modulators = m;
	return;
	}

void wave_add_channel(WAVE *w, SHAPE *amp) {
	amp->link = w->amplitudes;
	w->amplitudes = amp;
	return;
	}

WAVE *wave_new(SHAPE *carrier, SHAPE *amplitude) {
	WAVE *w;
	pthread_mutex_lock(&pool_mutex);
	if (pool == NULL) w = (WAVE *)malloc(sizeof(WAVE));
	else w = remove_wave(pool, &pool);
	pthread_mutex_unlock(&pool_mutex);
	w->modulators = NULL;
	w->carrier = carrier;
	w->amplitudes = NULL;
	if (amplitude == NULL) w->mute = 1;
	else {
		w->mute = 0;
		wave_add_channel(w, amplitude);
		}
	w->clock = 0;
	w->lead_in = 0;
	w->action_pending = 0;
	w->state_next = NULL;
	w->state_prev = NULL;
	w->status = WAVE_NEW;
	return w;
	}

static void release_wave(WAVE *w) {
	WAVE *mod, *next;
	w->status = WAVE_RELEASED;
	insert_wave(w, pool, INSERT_BEFORE, &pool);
	// release any modulators
	mod = w->modulators;
	while (mod != NULL) {
		next = mod->state_next;
		release_wave(mod);
		mod = next;
		}
	// release shapes
	if (w->carrier != NULL) shape_release(w->carrier);
	SHAPE *amp = w->amplitudes;
	SHAPE *nextamp;
	while (amp != NULL) {
		nextamp = amp->link;
		shape_release(amp);
		amp = nextamp;
		}
	}

static void invoke_action(WAVE *w) {
	char buf[128];
	lua_getglobal(w->lstate, action_ref_table);
	sprintf(buf, "%p", w);
	lua_pushstring(w->lstate, buf);
	lua_rawget(w->lstate, -2);
	lua_remove(w->lstate, -2);
	lua_pcall(w->lstate, 0, 0, 0);
	w->action_pending = 0;
//	lua_pop(w->lstate, 1);
	lua_getglobal(w->lstate, action_ref_table);
	lua_pushstring(w->lstate, buf);
	lua_pushnil(w->lstate);
	lua_rawset(w->lstate, -3);
	lua_pop(w->lstate, 1);
	return;
	}

static double sample_wave(WAVE *w) {
	if (w->lead_in > 0) {
		w->lead_in -= 1;
		return 0.0;
		}
	if (w->mute) {
		if (w->action_pending) invoke_action(w);
		return 0.0;
		}
	WAVE *mod;
	double modsum = 0.0;
	double t = w->clock / (double)sampling_rate;
	for (mod = w->modulators; mod != NULL; mod = mod->state_next) {
		modsum += shape_eval(mod->amplitudes, t) * sample_wave(mod);
		}
	double out = sin(shape_eval(w->carrier, t) * M_PI * 2.0 * t + modsum);
	w->clock += 1;
	return out;
	}

void release_active(WAVE *w) {
	if (w->status == WAVE_RELEASED) return;
//printf("release active %08lx\n", (unsigned long)w);
	pthread_mutex_lock(&active_mutex);
	remove_wave(w, &active_waves);
	pthread_mutex_unlock(&active_mutex);
	release_wave(w);
	return;
	}

void mix_waves(unsigned int channels, double *mixframe) {
	int i;
	for (i = 0; i < channels; i++) mixframe[i] = 0.0;
	WAVE *w = active_waves;
	while (w != NULL) {
		WAVE *next = w->state_next;
		double sample = sample_wave(w);
		if (w->mute) {
			if (!w->action_pending) release_active(w);
			w = next;
			continue;
			}
		int exhausted = 1;
		double t = w->clock / (double)sampling_rate;
		SHAPE *amp = w->amplitudes;
		for (i = channels - 1; i >= 0; i--) {
			if (!amp->exhausted) {
				mixframe[i] += shape_eval(amp, t) * sample;
				exhausted = 0;
				}
			amp = amp->link;
			if (amp == NULL) amp = w->amplitudes;
			}
		if (exhausted) {
			if (w->action_pending) invoke_action(w);
			release_active(w);
			}
		w = next;
		}
	return;
	}

static void activate_wave(WAVE *w, frame_t lead_in) {
	pthread_mutex_lock(&active_mutex);
	insert_wave(w, active_waves, INSERT_BEFORE, &active_waves);
	pthread_mutex_unlock(&active_mutex);
	w->status = WAVE_ACTIVE;
	w->lead_in = lead_in;
//printf("lead in %lld\n", w->lead_in);
	return;
	}

void activate_window(frame_t from, frame_t to) {
	WAVE *p = pending_waves;
	WAVE *next;
//	int init = 1;
	frame_t lead_in;
	while (p != NULL) {
		if (p->start >= to) break;
		next = p->state_next;
		if (p->start > from) lead_in = p->start - from;
		else {
			lead_in = 0;
//printf("lead in: %lld -> %lld\n", from, p->start);
			}
		pthread_mutex_lock(&pending_mutex);
		remove_wave(p, &pending_waves);
		pthread_mutex_unlock(&pending_mutex);
		activate_wave(p, lead_in);
/*
if (init) {
	printf("%10lld --------------------\n", from);
	init = 0;
	}
printf("activate %08llx\n", (unsigned long long)p);
*/
		p = next;
		}
	return;
	}

void wave_submit(WAVE *w, usec_t delay) {
	frame_t start = frame_clock +
		(frame_t)round((double)sampling_rate / 1000000.0 * delay);
	w->start = start;
	WAVE *p = pending_waves;
	WAVE *prev = NULL;
	WAVE *successor = NULL;
	while (p != NULL) {
		prev = p;
		if (w->start < p->start) {
			successor = p;
			break;
			}
		p = p->state_next;
		}
	pthread_mutex_lock(&pending_mutex);
	if (successor != NULL)
		insert_wave(w, successor, INSERT_BEFORE, &pending_waves);
	else
		insert_wave(w, prev, INSERT_AFTER, &pending_waves);
	pthread_mutex_unlock(&pending_mutex);
	return;
	}

static int wave_new_lua(lua_State *state) {
	WAVE *w = wave_new((SHAPE *)lua_touserdata(state, 1),
		(SHAPE *)lua_touserdata(state, 2));
	lua_pop(state, 2);
	lua_pushlightuserdata(state, (void *)w);
	return 1;
	}

static int wave_submit_lua(lua_State *state) {
	wave_submit((WAVE *)lua_touserdata(state, 1),
		(usec_t)lua_tonumber(state, 2));
	lua_pop(state, 2);
	return 0;
	}

static void set_action(WAVE *w) {
	// action function at top of stack
	char buf[128];
	sprintf(buf, "%p", w);
	lua_pushstring(w->lstate, buf);
	lua_insert(w->lstate, -2);
	lua_getglobal(w->lstate, action_ref_table);
	lua_insert(w->lstate, -3);
	lua_rawset(w->lstate, -3);
	lua_pop(w->lstate, 1);
	w->action_pending = 1;
	return;
	}

static int action_submit_lua(lua_State *state) {
	WAVE *w = wave_new(NULL, NULL);
	w->lstate = state;
	usec_t delay = (usec_t)lua_tonumber(state, 2);
	lua_remove(state, 2);
	set_action(w);
	w->mute = 1;
	wave_submit(w, delay);
	return 0;
	}

static int wave_add_on_done_lua(lua_State *state) {
	WAVE *w = (WAVE *)lua_touserdata(state, 1);
	w->lstate = state;
	lua_remove(state, 1);
	set_action(w);
	return 0;
	}

static int wave_add_channel_lua(lua_State *state) {
	wave_add_channel((WAVE *)lua_touserdata(state, 1),
			(SHAPE *)lua_touserdata(state, 2));
	lua_pop(state, 2);
	return 0;
	}

static int wave_add_modulator_lua(lua_State *state) {
	wave_add_modulator((WAVE *)lua_touserdata(state, 1),
			(WAVE *)lua_touserdata(state, 2));
	lua_pop(state, 2);
	return 0;
	}

void wave_init(lua_State *lstate) {
	pthread_mutex_init(&pool_mutex, NULL);
	pthread_mutex_init(&active_mutex, NULL);
	pthread_mutex_init(&pending_mutex, NULL);
	lua_pushcfunction(lstate, wave_new_lua);
	lua_setglobal(lstate, "wave_new");
	lua_pushcfunction(lstate, wave_add_channel_lua);
	lua_setglobal(lstate, "wave_add_channel");
	lua_pushcfunction(lstate, wave_add_modulator_lua);
	lua_setglobal(lstate, "wave_add_modulator");
	lua_pushcfunction(lstate, wave_add_on_done_lua);
	lua_setglobal(lstate, "wave_add_on_done");
	lua_pushcfunction(lstate, wave_submit_lua);
	lua_setglobal(lstate, "wave_submit");
	lua_pushcfunction(lstate, action_submit_lua);
	lua_setglobal(lstate, "action_submit");
	lua_newtable(lstate);
	lua_setglobal(lstate, action_ref_table);
	}
