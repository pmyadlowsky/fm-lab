#ifndef _SHAPE_H
#define _SHAPE_H

#include <lua.h>
#include "segment.h"

typedef struct shape {
	double mark_x;
	double mark_y;
	SEGMENT *last_seg;
	SEGMENT *segs;
	SEGMENT *current_seg;
	struct shape *link;
	int exhausted;
	} SHAPE;

void shape_init(lua_State *);
SHAPE *shape_new(double, double);
SHAPE *shape_static(double);
void shape_add_segment(SHAPE *, double, double, int);
void shape_release(SHAPE *);
void show_shape(SHAPE *);
double shape_eval(SHAPE *, double);
#endif
