#ifndef _JACK_H
#define JACK_H

#include <lua.h>
#include "shape.h"

extern unsigned int sampling_rate;

int jack_init(int, lua_State *);
void jack_run(void);
void jack_shutdown(void);
void jack_set_volume(SHAPE *);
#endif
