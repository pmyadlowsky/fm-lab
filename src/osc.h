#ifndef _OSC_H
#define _OSC_H

#include <lua.h>

void osc_init(lua_State *, int);
void osc_shutdown(void);
int osc_get_fd(void);
void osc_process(void);
#endif
