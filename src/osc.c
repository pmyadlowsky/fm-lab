#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <lo/lo.h>
#include <lua.h>
#include "osc.h"
#include "jack.h"
#include "shape.h"

static lo_server osc_server = NULL;
static int osc_fd = -1;
static pthread_mutex_t osc_mutex;

void osc_shutdown(void) {
	if (osc_server != NULL) {
		lo_server_free(osc_server);
		osc_server = NULL;
		}
	return;
	}

static void osc_error(int num, const char *msg, const char *path) {
	printf("OSC server error %d in path %s: %s\n", num, path, msg);
	osc_shutdown();
	return;
	}

static int osc_test_handler(const char *path,
		const char *types, lo_arg **argv, int argc,
		void *data, void *user_data) {
	printf("OSC TEST: %s\n", path);
	return 0;
	}

static int osc_volume_handler(const char *path,
		const char *types, lo_arg **argv, int argc,
		void *data, void *user_data) {
	jack_set_volume(shape_static((double)argv[0]->f));
	return 0;
	}

static int lua_call_handler(const char *path,
		const char *types, lo_arg **argv, int argc,
		void *data, void *user_data) {
	lua_State *lstate = (lua_State *)user_data;
	lua_getglobal(lstate, &path[1]);
	if (!lua_isfunction(lstate, 1)) {
		printf("method '%s' is not a Lua function\n", &path[1]);
		lua_pop(lstate, 1);
		return 0;
		}
	int i;
	int argn = 0;
	for (i = 0; i < argc; i++) {
		if (types[i] == 's') {
			lua_pushstring(lstate, (const char *)&(argv[i]->c));
			argn += 1;
			}
		else if (types[i] == 'f') {
			lua_pushnumber(lstate, (float)argv[i]->f);
			argn += 1;
			}
		else if (types[i] == 'i') {
			lua_pushinteger(lstate, (int)argv[i]->i32);
			argn += 1;
			}
		}
	lua_pcall(lstate, argn, 0, 0);
	return 0;
	}

static void osc_register(const char *fun_name, const char *spec,
							lua_State *state) {
	char buf[128];
	sprintf(buf, "/%s", fun_name);
	printf("OSC register '%s'\n", buf);
	lo_server_add_method(osc_server, buf, spec,
		lua_call_handler, (void *)state);
	return;
	}

static int osc_register_lua(lua_State *state) {
	osc_register(lua_tostring(state, 1), lua_tostring(state, 2), state);
	lua_pop(state, 2);
	return 0;
	}

void osc_init(lua_State *lstate, int port) {
	pthread_mutex_init(&osc_mutex, NULL);
	char buf[16];
	sprintf(buf, "%d", port);
	osc_server = lo_server_new_with_proto(buf, LO_UDP, osc_error);
	osc_fd = lo_server_get_socket_fd(osc_server);
	lo_server_add_method(osc_server, "/test", "", osc_test_handler, NULL);
	lo_server_add_method(osc_server, "/vol", "f", osc_volume_handler, NULL);
	lua_pushcfunction(lstate, osc_register_lua);
	lua_setglobal(lstate, "osc_register");
	printf("OSC listening at %s\n", lo_server_get_url(osc_server));
	return;
	}

int osc_get_fd(void) {
	return osc_fd;
	}

void osc_process(void) {
	int recv;
	while (1) {
		pthread_mutex_lock(&osc_mutex);
		recv = lo_server_recv_noblock(osc_server, 0);
		pthread_mutex_unlock(&osc_mutex);
		if (recv < 1) break;
		}
	return;
	}
