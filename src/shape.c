#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include "shape.h"
#include "segment.h"
#include "vertex.h"

static SHAPE *pool = NULL;
static pthread_mutex_t pool_mutex;

SHAPE *shape_new(double x, double y) {
	SHAPE *shape;
	pthread_mutex_lock(&pool_mutex);
	if (pool == NULL) {
		shape = (SHAPE *)malloc(sizeof(SHAPE));
		}
	else {
		shape = pool;
		pool = shape->link;
		}
	pthread_mutex_unlock(&pool_mutex);
	shape->mark_x = x;
	shape->mark_y = y;
	shape->last_seg = NULL;
	shape->segs = NULL;
	shape->current_seg = NULL;
	shape->link = NULL;
	shape->exhausted = 0;
	return shape;
	}

SHAPE *shape_static(double val) {
	return shape_new(0.0, val);
	}

void shape_add_segment(SHAPE *shape,
		double finish_x, double finish_y, int form) {
	SEGMENT *seg;
	VERTEX *vstart, *vfinish;
	if (shape->last_seg == NULL) {
		vstart = get_vertex(shape->mark_x, shape->mark_y);
		}
	else {
		vstart = shape->last_seg->finish;
		}
	vfinish = get_vertex(finish_x, finish_y);
	seg = get_segment(vstart, vfinish, form, shape->last_seg == NULL);
	seg->seq = NULL;
	if (shape->last_seg == NULL) {
		shape->segs = seg;
		shape->current_seg = seg;
		}
	else shape->last_seg->seq = seg;
	shape->last_seg = seg;
	return;
	}

void shape_reset(SHAPE *shape) {
	shape->current_seg = shape->segs;
	return;
	}

double shape_eval(SHAPE *shape, double x) {
	double val;
	int n;
	if (shape->segs == NULL) return shape->mark_y;
	while (1) {
		if (shape->current_seg == NULL) return shape->mark_y;
		n = (*shape->current_seg->interpolator)(shape->current_seg, x, &val);
		if (n) {
			shape->mark_x = x;
			shape->mark_y = val;
			return val;
			}
		shape->current_seg = shape->current_seg->seq;
		if (shape->current_seg == NULL) shape->exhausted = 1;
		}
	}
void shape_release(SHAPE *shape) {
	SEGMENT *seg;
	pthread_mutex_lock(&pool_mutex);
	shape->link = pool;
	pool = shape;
	pthread_mutex_unlock(&pool_mutex);
	for (seg = shape->segs; seg != NULL; seg = seg->seq) {
		release_segment(seg);
		}
	return;
	}

void show_shape(SHAPE *shape) {
	SEGMENT *seg;
	for (seg = shape->segs; seg != NULL; seg = seg->seq) {
		printf("[%1.3f, %1.3f] -> [%1.3f, %1.3f]\n",
			seg->start->x, seg->start->y,
			seg->finish->x, seg->finish->y);
		}
	return;
	}

static int shape_new_lua(lua_State *state) {
	SHAPE *s = shape_new((double)lua_tonumber(state, 1),
		(double)lua_tonumber(state, 2));
	lua_pop(state, 2);
	lua_pushlightuserdata(state, (void *)s);
	return 1;
	}

static int shape_static_lua(lua_State *state) {
	SHAPE *s = shape_static((double)lua_tonumber(state, 1));
	lua_pop(state, 1);
	lua_pushlightuserdata(state, (void *)s);
	return 1;
	}

static int shape_add_segment_lua(lua_State *state) {
	shape_add_segment(
		(SHAPE *)lua_touserdata(state, 1),
		(double)lua_tonumber(state, 2),
		(double)lua_tonumber(state, 3),
		(int)lua_tonumber(state, 4));
	lua_pop(state, 4);
	return 0;
	}

void shape_init(lua_State *lstate) {
	pthread_mutex_init(&pool_mutex, NULL);
	lua_pushinteger(lstate, SEGMENT_LINEAR);
	lua_setglobal(lstate, "SEGMENT_LINEAR");
	lua_pushinteger(lstate, SEGMENT_EXPONENTIAL);
	lua_setglobal(lstate, "SEGMENT_EXPONENTIAL");
	lua_pushinteger(lstate, SEGMENT_SIN2);
	lua_setglobal(lstate, "SEGMENT_SIN2");
	lua_pushcfunction(lstate, shape_new_lua);
	lua_setglobal(lstate, "shape_new");
	lua_pushcfunction(lstate, shape_static_lua);
	lua_setglobal(lstate, "shape_static");
	lua_pushcfunction(lstate, shape_add_segment_lua);
	lua_setglobal(lstate, "shape_add_segment");
	return;
	}
