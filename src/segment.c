#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <pthread.h>
#include "segment.h"

static SEGMENT *pool = NULL;
static pthread_mutex_t pool_mutex;
static int need_init = 1;

static void init_segment() {
	need_init = 0;
	pthread_mutex_init(&pool_mutex, NULL);
	return;
	}

inline int out_of_range(SEGMENT *s, double x) {
	return ((x < s->start->x) || (x >= s->finish->x));
	}

inline double reach(SEGMENT *s, double x) {
	return (x - s->start->x) / s->width;
	}

static int linear(SEGMENT *s, double x, double *res) {
	if (out_of_range(s, x)) return 0;
	*res = reach(s, x) * s->height + s->start->y;
	return 1;
	}

static int exponential(SEGMENT *s, double x, double *res) {
	if (out_of_range(s, x)) return 0;
	*res = s->start->y * pow(s->finish->y / s->start->y, reach(s, x));
	return 1;
	}

static int sin2(SEGMENT *s, double x, double *res) {
	if (out_of_range(s, x)) return 0;
	double sn = sin(reach(s, x) * M_PI_2);
	*res = sn * sn * s->height + s->start->y;
	return 1;
	}

SEGMENT *get_segment(VERTEX *start, VERTEX *finish, int form, int first) {
	SEGMENT *s;
	if (need_init) init_segment();
	pthread_mutex_lock(&pool_mutex);
	if (pool == NULL) {
		s = (SEGMENT *)malloc(sizeof(SEGMENT));
		}
	else {
		s = pool;
		pool = s->link;
		}
	pthread_mutex_unlock(&pool_mutex);
	s->start = start;
	s->finish = finish;
	s->form = form;
	switch(form) {
		case SEGMENT_LINEAR:
			s->interpolator = linear;
			break;
		case SEGMENT_EXPONENTIAL:
			s->interpolator = exponential;
			break;
		case SEGMENT_SIN2:
			s->interpolator = sin2;
			break;
		default:
			s->interpolator = linear;
			break;
		}
	s->first = first;
	s->link = NULL;
	s->seq = NULL;
	s->width = s->finish->x - s->start->x;
	s->height = s->finish->y - s->start->y;
	return s;
	}

void release_segment(SEGMENT *s) {
	pthread_mutex_lock(&pool_mutex);
	s->link = pool;
	pool = s;
	pthread_mutex_unlock(&pool_mutex);
	if (s->first) release_vertex(s->start);
	release_vertex(s->finish);
	return;
	}
